package de.franden.cdiconfig.control;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface ConfigProperty {

    public static final String NOT_SET = "NOT_SET";

    @Nonbinding
    String name() default NOT_SET;

    @Nonbinding
    String defaultValue() default NOT_SET;
}
