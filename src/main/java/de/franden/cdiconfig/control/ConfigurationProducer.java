package de.franden.cdiconfig.control;

import org.jboss.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.bean.RequestScoped;
import java.util.Optional;

@RequestScoped
public class ConfigurationProducer {

    Logger logger = Logger.getLogger(getClass());

    public String produceConfiguration(InjectionPoint ip) {
        Annotated annotated = ip.getAnnotated();
        ConfigProperty annotation = annotated.getAnnotation(ConfigProperty.class);
        if (annotation != null) {
            String key = annotation.name();
            String propertyValue = System.getProperty(key);
            if(propertyValue == null && !ConfigProperty.NOT_SET.equals(annotation.defaultValue())) {
                propertyValue = annotation.defaultValue();
            }
            logger.info("configuration value was found key: " + key + " value:" + propertyValue);
            return propertyValue;
        } else {
            return null;
        }
    }

    @ConfigProperty
    @Produces
    public String produceStringConfiguration(InjectionPoint ip) {
        String property = produceConfiguration(ip);
        if (property == null) {
            return null;
        } else {
            return property.toString();
        }
    }

    @ConfigProperty
    @Produces
    public Long produceLongConfiguration(InjectionPoint ip) {
        String property = produceConfiguration(ip);
        if (property == null) {
            return null;
        } else {
            Long parsedLong = null;
            if (property != null) {
                parsedLong = Long.parseLong(property);
            }
            return parsedLong;
        }
    }

    @ConfigProperty
    @Produces
    public Optional<Long> produceLongOptionalConfiguration(InjectionPoint ip) {
        return Optional.ofNullable(produceLongConfiguration(ip));
    }

}
