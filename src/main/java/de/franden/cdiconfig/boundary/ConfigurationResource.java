package de.franden.cdiconfig.boundary;

import de.franden.cdiconfig.control.ConfigProperty;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.LocalTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

@Path("/configuration")
@Produces(MediaType.APPLICATION_JSON)
public class ConfigurationResource {

    public static final String EMPTY_STRING = "";
    public static final Long EMPTY_LONG = 999999999999999L;

    @Inject
    @ConfigProperty
    private String noName;

    @Inject
    @ConfigProperty(name = "de.franden.cdiconfig.boundary.color")
    private String colorProperty;

    @Inject
    @ConfigProperty(name = "de.franden.cdiconfig.boundary.weather")
    private String weatherProperty;

    @Inject
    @ConfigProperty(name = "de.franden.cdiconfig.boundary.answer")
    private Long answerProperty;

    @Inject
    @ConfigProperty(name = "de.franden.cdiconfig.boundary.size")
    private Optional<Long> sizeProperty;

    @Inject
    @ConfigProperty(name = "de.franden.cdiconfig.boundary.birthyear", defaultValue = "1986")
    private Optional<Long> birthYearProperty;


    @GET
    @Path("properties")
    public String getProperties() {
        return getSortedJson(System.getProperties().entrySet()).toString();
    }

    @GET
    @Path("properties/custom")
    public String getCustomProperties() {
        JsonObject configuration = Json.createObjectBuilder()
                .add("noName", firstNotNull(noName, EMPTY_STRING))
                .add("colorProperty", firstNotNull(colorProperty, EMPTY_STRING))
                .add("weatherProperty", firstNotNull(weatherProperty, EMPTY_STRING))
                .add("answerProperty", firstNotNull(answerProperty, EMPTY_LONG))
                .add("sizeProperty", sizeProperty.orElse(EMPTY_LONG))
                .add("birthYearProperty", birthYearProperty.orElse(EMPTY_LONG))
                .build();
        JsonObject build = Json.createObjectBuilder()
                .add("response", "Hello from WildFly Swarm!")
                .add("time", LocalTime.now().toString())
                .add("configuration", configuration)
                .build();
        return build.toString();
    }

    <T> T firstNotNull(T first, T second) {
        if (first == null) {
            return second;
        } else {
            return first;
        }
    }

    @GET
    @Path("environment")
    public String all() {
        return getSortedJson(System.getenv().entrySet()).toString();
    }

    <T> JsonObject getSortedJson(Set<Map.Entry<T, T>> entries) {
        Map<Object, Object> sortedObjects = new TreeMap<>();
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        entries.forEach(entry -> sortedObjects.put(entry.getKey(), entry.getValue()));

        sortedObjects.entrySet().forEach(sortedEntry -> objectBuilder.add(sortedEntry.getKey().toString(), sortedEntry.getValue().toString()));
        return objectBuilder.build();
    }
}