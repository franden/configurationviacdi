package de.franden.cdiconfig.boundary;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class JAXRSConfiguration extends Application {

}
